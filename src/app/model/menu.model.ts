export interface MenuModel {
  url: string;
  name: string;
  minumAge: number;
}
