import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {MoodService} from '../../service/mood.service';

@Component({
  selector: 'app-mood',
  templateUrl: './mood.component.html',
  styleUrls: ['./mood.component.scss']
})
export class MoodComponent implements OnInit {
  constructor(
    private moodService: MoodService
  ) { }

  ngOnInit(): void {
  }
  Iok() {
    this.moodService.setMood('je suis ok');
  }
  NotOk() {
    this.moodService.setMood('je ne suis pas ok');
  }
  Neutre() {
    this.moodService.setMood('je suis neutre');
  }

}
