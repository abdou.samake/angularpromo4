import {Component, Input, OnInit} from '@angular/core';
import {MenuModel} from '../../../model/menu.model';
import {MenuService} from '../../../service/menu.service';

@Component({
  selector: 'app-menu1',
  templateUrl: './menu1.component.html',
  styleUrls: ['./menu1.component.scss']
})
export class Menu1Component implements OnInit {
  name = '';
  routes: MenuModel[];
  constructor(
    private menuService: MenuService
  ) {
  }

  ngOnInit(): void {
    this.name = 'Jean Paul';
    this.routes = this.menuService.allRoutes;
  }

  sayHello() {
    return {name: 'abdou', age: 28};
  }
  @Input() age: number;

}

