import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MoodService {
  handleMood$: BehaviorSubject<string> = new BehaviorSubject<string>(null);

  constructor() { }
  setMood(message) {
    this.handleMood$.next(message);
  }
}
