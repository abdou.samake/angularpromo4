import { Injectable } from '@angular/core';
import {MenuModel} from '../model/menu.model';

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  allRoutes: MenuModel[] = [
    {url: 'home', name: 'accueil', minumAge: 0},
    {url: 'à-propos', name: 'à propos', minumAge: 40},
    {url: 'form', name: 'formulaire', minumAge: 2}
  ];
  constructor() { }
}
